/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arukkha.swingproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Coke
 */
public class WriteFriend {
    public static void main(String[] args) {
        ArrayList<Friend> friends = new ArrayList();
        friends.add(new Friend("Arukha" , 20, "Male" , "Best Friend"));
        friends.add(new Friend("Watsapon" , 20, "Male" , "Best Friend"));
        friends.add(new Friend("JIJI" , 20, "Male" , "Best Friend"));

        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos =null;
        try {
            file = new File("friend.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(friends);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
}
